# tytesthelpers - A Unity project/module providing simple mesh and shape renderers.

## Design

Goal is to make some of the Unity mesh handling and rendering a bit more streamlined and 
easy to use.

A renderer class is meant to store a mesh and know how to render it using Unity.  
There's a ITestRenderer interface that defines common interactions with any renderer, including
things like rendering (output to Unity) and clearing/flushing.  Other actions are renderer
specific.

Two renderer implementations are provided.  **TestRenderer** ties meshdata (my internal
structure for dealing with meshes) to the renderer interface.  Useful for simplifying
the handling of anything that uses the meshdata.  **TestShapeRenderer** ties simple
shape primitives (like drawing lines, points, circles, etc.) to the renderer interface.

There's also a **TestRendererSet** that acts a collection of renderers and retains the 
same renderer interface.

## Dependencies
* [tymesh](https://bitbucket.org/ptjal/tymesh)
* [tytest](https://bitbucket.org/ptjal/tysimpleshape)

## Usage

To do...

