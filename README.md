# tytesthelpers - A Unity project/module providing simple mesh and shape renderers.

## Install
Add the following to Packages/manifest.json
```
    "com.ptjal.tymesh": "https://bitbucket.org/ptjal/tymesh.git#0.1.0",
    "com.ptjal.tysimpleshape": "https://bitbucket.org/ptjal/tysimpleshape.git#0.1.0",
    "com.ptjal.tytesthelpers": "https://bitbucket.org/ptjal/tytesthelpers.git#0.1.0",
```

## Package Info
[See Package Info Here](Assets/tytesthelpers)
